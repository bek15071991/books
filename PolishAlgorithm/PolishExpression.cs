﻿using System;
using System.Linq;
using JetBrains.Annotations;

namespace PolishAlgorithm
{
    internal abstract class PolishExpression
    {
        public static PolishExpression Create([NotNull] string value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (value.Any(d => !char.IsDigit(d)))
            {
                return new PolishOperator(value);
            }

            return new PolishOperand(value);
        }
    }

    internal class PolishOperator : PolishExpression
    {
        private readonly string _value;

        public PolishOperator([NotNull] string value)
        {
            _value = value ?? throw new ArgumentNullException(nameof(value));
        }

        public PolishOperand Calculate(int a, int b)
        {
            var result = 0;

            switch (_value)
            {
                case "-":
                    result = a - b;
                    break;
                case "+":
                    result = a + b;
                    break;
                case "*":
                    result = a * b;
                    break;
                case "/":
                    result = a / b;
                    break;
            }

            return new PolishOperand(result);
        }
    }

    internal class PolishOperand : PolishExpression
    {
        public int Value { get; }

        public PolishOperand([NotNull] string value)
        {
            if (value == null) throw new ArgumentNullException(nameof(value));

            if (!int.TryParse(value, out var parsedValue))
                throw new ArgumentException("Value must be int type");

            Value = parsedValue;
        }

        public PolishOperand(int value)
        {
            Value = value;
        }
    }
}