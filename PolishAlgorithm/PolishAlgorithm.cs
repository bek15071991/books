﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace PolishAlgorithm
{
    public class PolishAlgorithm
    {
        public int Calculate([NotNull] string value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            var stack = new Stack<int>();

            var parsedValues = Parse(value);

            foreach (var polishExpression in parsedValues)
            {
                if (polishExpression is PolishOperand operand)
                {
                    stack.Push(operand.Value);
                    continue;
                }

                var @operator = (PolishOperator)polishExpression;
                var firstOperand = stack.Pop();
                var secondOperand = stack.Pop();

                var result = @operator.Calculate(secondOperand, firstOperand);

                stack.Push(result.Value);
            }

            return stack.Pop();
        }

        private static IEnumerable<PolishExpression> Parse(string value)
        {
            var values = value.Split(' ');

            return values.Select(PolishExpression.Create);
        } 
    }
}