﻿using System.Linq;
using System.Web.Http;
using Books.Core;
using Books.Models;

namespace Books.Controllers
{
    [RoutePrefix("api/books")]
    public class BookController : ApiController
    {
        [Route(""), HttpPost]
        public IHttpActionResult Add(BookModel bookModel)
        {
            if (bookModel == null)
                return BadRequest(nameof(bookModel));

            var authors = bookModel.Authors.Select(d => new Author(d.FirstName, d.SecondName));

            var book = new Book(
                BooksStorage.NextId, 
                bookModel.Title,
                bookModel.PagesCount,
                bookModel.ISBN,
                authors);

            BooksStorage.Books.Add(book);

            return Ok();
        }

        [Route("{id}"), HttpDelete]
        public IHttpActionResult Remove(int id)
        {
            var bookForDelete = BooksStorage.Books.FirstOrDefault(d => d.Id == id);

            if (bookForDelete == null)
                return BadRequest($"Cannot found book #{id}");

            BooksStorage.Books.Remove(bookForDelete);

            return Ok();
        }

        [Route(""), HttpPut]
        public IHttpActionResult Edit(BookModel bookModel)
        {
            if (bookModel == null)
                return BadRequest(nameof(bookModel));

            var bookForEdit = BooksStorage.Books.FirstOrDefault(b => b.Id == bookModel.Id);

            if (bookForEdit == null)
                return BadRequest($"Cannot found book #{bookModel.Id}");

            bookForEdit.ISBN = bookModel.ISBN;
            bookForEdit.PagesCount = bookModel.PagesCount;
            bookForEdit.PublicationYear = bookModel.PublicationYear;
            bookForEdit.PublisherName = bookModel.PublisherName;
            bookForEdit.Title = bookModel.Title;
            bookForEdit.Authors = bookModel.Authors.Select(d => new Author(d.FirstName, d.SecondName)).ToList();


            return Ok();
        }

        [Route(""), HttpGet]
        public IHttpActionResult Get(SortModel sortModel)
        {
            if (sortModel == null)
                return Ok(BooksStorage.Books);

            var sortedBooks = BooksStorage.Books.ToList();

            var bookComparer = new BookComparer(sortModel); 

            sortedBooks.Sort(bookComparer);
            
            return Ok(sortedBooks);
        }
         
    }
}