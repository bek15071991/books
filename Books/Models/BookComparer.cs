﻿using System;
using System.Collections.Generic;
using Books.Core;
using JetBrains.Annotations;

namespace Books.Models
{
    public class BookComparer : IComparer<Book>
    {
        private readonly SortModel _sortModel;

        public BookComparer([NotNull] SortModel sortModel)
        {
            _sortModel = sortModel ?? throw new ArgumentNullException(nameof(sortModel));
        }

        public int Compare(Book x, Book y)
        {
            if (x == null)
                throw new ArgumentNullException(nameof(x));

            if (y == null)
                throw new ArgumentNullException(nameof(y));


            if (_sortModel.PublisherYear)
            {
                if (x.PublicationYear == y.PublicationYear)
                    return 0;

                if (x.PublicationYear == null)
                    return -1;

                if (y.PublicationYear == null)
                    return 1;

                return x.PublicationYear.Value.CompareTo(y.PublicationYear.Value);
            }


            return string.Compare(x.Title, y.Title, StringComparison.Ordinal);
        }
    }
}