﻿namespace Books.Models
{
    public class SortModel
    {
        private bool _publisherYear;

        public bool PublisherYear
        {
            get => _publisherYear;
            set
            {
                _publisherYear = value;

                if (value)
                    _title = false;
            }
        }

        private bool _title;

        public bool Title
        {
            get => _title;
            set
            {
                _title = value;
                if (value)
                    _publisherYear = false;
            }
        }
    }
}