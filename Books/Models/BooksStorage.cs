﻿using System;
using System.Collections.Generic;
using Books.Core;

namespace Books.Models
{
    public static class BooksStorage
    {
        public static IList<Book> Books = new List<Book>();

        private static int _id = 1;

        public static int NextId => _id++;

        static BooksStorage()
        {
            Books.Add(new Book(
                NextId,
                "Язык программирования C# 5.0 и платформа .NET 4.5",
                1312,
                "9785845918147",
                new[] {new Author("Эндрю", "Троелсен"), new Author("Филипп", "Джепикс")})
            {
                PublicationYear = new DateTime(2015, 1, 1)
            });

            Books.Add(new Book(
                NextId,
                "C# 5.0 и платформа .NET 4.5 для профессионалов",
                1440,
                "978-1-1183-1442-5",
                new[] {new Author("Кристиан", "Нагел"), new Author("Билл", "Ивьен"), new Author("Морган", "Скиннер")})
            {
                PublicationYear = new DateTime(2015, 1, 1)
            });

            Books.Add(new Book(
                NextId,
                "Шаблоны реализации корпоративных приложений",
                176,
                "9785845914064",
                new[] {new Author("Кент", "Бек")})
            {
                PublicationYear = new DateTime(2015, 1, 1)
            });

            Books.Add(new Book(
                NextId,
                "CLR via C#.",
                896,
                "9780735642812",
                new[] {new Author("Джеффри ", "Рихтер")})
            {
                PublicationYear = new DateTime(2015, 1, 1)
            });
        }
    }
}