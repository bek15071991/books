﻿using System;
using System.Collections.Generic;

namespace Books.Models
{
    public class BookModel
    {
        public int Id { get; set; }

        public int PagesCount { get; set; }

        public string Title { get; set; }

        public string ISBN { get; set; }

        public string PublisherName { get; set; }

        public DateTime? PublicationYear { get; set; }

        public IList<BookAuthor> Authors { get; set; } 
    }

    public class BookAuthor
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string SecondName { get; set; }
    }
}