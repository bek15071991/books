﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace Books.Core
{
    public class Book
    {
        public int Id { get; }

        private string _title;

        public string Title
        {
            get => _title;
            set
            {
                if (value.Length < 30)
                    throw new ArgumentException("Length  of title cannot be less 30 characters");

                _title = value ?? throw new ArgumentNullException(nameof(Title));
            }
        }

        private string _publisherName;

        public string PublisherName
        {
            get => _publisherName;
            set
            {
                if (value != null && value.Length < 30)
                    throw new ArgumentException("Length of publisher name cannot be less 30 characters");

                _publisherName = value;
            }
        }

        private int _pagesCount;

        public int PagesCount
        {
            get => _pagesCount;
            set
            {
                if (value < 1)
                    throw new ArgumentException("Count of pages cannot be less 1");

                if (value > 10000)
                    throw new ArgumentException("Count of pages cannot be more 10000");

                _pagesCount = value;
            }
        }

        private string _isbn;

        public string ISBN
        {
            get => _isbn;
            set => _isbn = value ?? throw new ArgumentNullException(nameof(Title));
        }

        private DateTime? _publicationYear;

        public DateTime? PublicationYear
        {
            get => _publicationYear;
            set
            {
                if (value != null && value.Value.Year < 1800)
                    throw new ArgumentException("Publication year cannot be not earlier 1800");

                _publicationYear = value;
            }
        }

        private IList<Author> _authors;

        public IList<Author> Authors
        {
            get => _authors;
            set
            {
                if (value == null)
                    throw new ArgumentNullException(nameof(Authors));

                if (!value.Any())
                    throw new ArgumentException("Book must have at least one author");

                _authors = value;
            }
        }

        public Book(int id,
            [NotNull] string title,
            int pagesCount,
            [NotNull] string isbn,
            [NotNull] IEnumerable<Author> authors)
        {
            Id = id;
            Title = title ?? throw new ArgumentNullException(nameof(title));
            PagesCount = pagesCount;
            ISBN = isbn;
            Authors = authors.ToList();
        }
    }
}