﻿using System;

namespace Books.Core
{
    public class Author
    {
        public string FirstName { get; }

        public string SecondName { get; }

        public Author(string firstName, string secondName)
        {
            FirstName = firstName ?? throw new ArgumentNullException(nameof(firstName));
            SecondName = secondName ?? throw new ArgumentNullException(nameof(secondName));
        }
    }
}