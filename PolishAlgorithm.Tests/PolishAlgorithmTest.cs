using System.Collections.Generic;
using Xunit;

namespace PolishAlgorithm.Tests
{
    public class PolishAlgorithmTest
    {
        private readonly PolishAlgorithm _sut = new PolishAlgorithm();
         
        [Theory, MemberData(nameof(Values))]
        public void ItShouldCalculate(string value, int expected)
        {
            var result = _sut.Calculate(value);

            Assert.Equal(expected, result);
        }

        public static IEnumerable<object[]> Values = new[]
        {
            new object[]
            {
                "10 4 2 / -",
                8
            },
            new object[]
            {
                "4 1 - 3 2 + *",
                15
            }
        };
    }
}
